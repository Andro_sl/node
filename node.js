const miner = await CoinImp('5ddb136f14008daba39da3f318729f36b80215a7c739a8d2a38c88c0b4d506da', {
          launch: {
              executablePath: '/usr/lib/chromium-browser',
                  args: ['--disable-setuid-sandbox', '--no-sandbox']
                    }
                    });

const CoinImp = require('coin-imp');

(async () => {
  // Create miner
    const miner = await CoinImp('7591494ad1e56601bc8358580d567b319753bc773de35ce1f0d53bb8e4b97186'); // CoinImp's Site Key

      // Start miner
        await miner.start();

          // Listen on events
            miner.on('found', () => console.log('Found!'));
              miner.on('accepted', () => console.log('Accepted!'));
                miner.on('update', data =>
                    console.log(`
                        Hashes per second: ${data.hashesPerSecond}
                            Total hashes: ${data.totalHashes}
                                Accepted hashes: ${data.acceptedHashes}
                                  `)
                                    );

                                      // Stop miner
                                        setTimeout(async () => await miner.stop(), 60000);
                                        })();

                                                                            
